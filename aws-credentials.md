## Creating an AWS Credentials File

Create a file in `~/.aws/credentials`

Add this section:

```
[chess]
aws_access_key_id = <key_id>
aws_secret_access_key = <secret_key>
region=us-east-2
```