import sys
import time
import yaml

import boto3
from botocore.exceptions import ClientError

configFileNames = [
    "/Users/nohr/Documents/ParallelWindows/AWS-Stockfish/engine.yml",
    "/Users/nohr/Documents/ParallelWindows/AWS-Leela/engine.yml"
]
shutdownTimeout = 15
# configFileName = "SSHEngine/engine.yml"

session = boto3.session.Session(profile_name='chess')
ec2 = session.client('ec2')


def getFirstInstanceId():
    response = ec2.describe_instances()
    instances = response["Reservations"][0]["Instances"]
    for nextInstance in instances:
        return nextInstance["InstanceId"]


def getSingleInstanceInformation(instanceId):
    response = ec2.describe_instances(InstanceIds=[instanceId], DryRun=False)
    instance = response["Reservations"][0]["Instances"][0]

    print("InstanceId:" + instance["InstanceId"])
    print("Type: " + instance["InstanceType"])
    print("State: " + instance["State"]["Name"])
    if "PublicIpAddress" in instance:
        print("IP Address: " + instance["PublicIpAddress"])


def startInstance(instanceId):
    # Do a dryrun first to verify permissions
    try:
        ec2.start_instances(InstanceIds=[instanceId], DryRun=True)
    except ClientError as e:
        if 'DryRunOperation' not in str(e):
            raise

    # Dry run succeeded, run start_instances without dryrun
    try:
        response = ec2.start_instances(InstanceIds=[instanceId], DryRun=False)
        # print(response)
    except ClientError as e:
        print(e)


def stopInstance(instanceId):
    # Do a dryrun first to verify permissions
    try:
        ec2.stop_instances(InstanceIds=[instanceId], DryRun=True)
    except ClientError as e:
        if 'DryRunOperation' not in str(e):
            raise

    # Dry run succeeded, call stop_instances without dryrun
    try:
        response = ec2.stop_instances(InstanceIds=[instanceId], DryRun=False)
        # print(response)
    except ClientError as e:
        print(e)


def updateConfig(instanceId, fileName):
    response = ec2.describe_instances(InstanceIds=[instanceId], DryRun=False)
    instance = response["Reservations"][0]["Instances"][0]
    if "PublicIpAddress" in instance:
        ipAddress = instance["PublicIpAddress"]

        my_config = read_yaml(fileName)
        my_config["host"] = ipAddress
        write_yaml(my_config, fileName)


def read_yaml(fileName):
    """ A function to read YAML file"""
    with open(fileName) as f:
        config = yaml.safe_load(f)

    return config


def write_yaml(data, fileName):
    """ A function to write YAML file"""
    with open(fileName, 'w') as f:
        yaml.dump(data, f)


def printUsage():
    print("Usage:")
    print("\tec2-manager start\tstarts the instance")
    print("\tec2-manager stop\tstops the instance")
    print("\tec2-manager info\tGet details about the instance")
    print("\tec2-manager config\tUpdate the configuration file")


def mainStart():
    print("Starting instance...")
    instanceId = getFirstInstanceId()
    startInstance(instanceId)
    print("Waiting 10 seconds for IP...")
    time.sleep(10)
    getSingleInstanceInformation(instanceId)


def mainStop():
    print("Stopping instance...")
    instanceId = getFirstInstanceId()
    stopInstance(instanceId)
    print("Waiting a few seconds to check status...")
    time.sleep(5)
    getSingleInstanceInformation(instanceId)


def mainConfig():
    print("Configuring...")
    instanceId = getFirstInstanceId()
    for configFileName in configFileNames:
        print("Updating: " + configFileName)
        updateConfig(instanceId, configFileName)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        printUsage()
    elif sys.argv[1] == "info":
        print("Getting instance information...")
        instanceId = getFirstInstanceId()
        getSingleInstanceInformation(instanceId)
    elif sys.argv[1] == "start":
        mainStart()
    elif sys.argv[1] == "stop":
        mainStop()
    elif sys.argv[1] == "config":
        mainConfig()
    elif sys.argv[1] == "startconfig":
        mainStart()
        mainConfig()
    elif sys.argv[1] == "fullstart":
        mainStart()
        mainConfig()

        print("Will shut down in ", shutdownTimeout, " minutes")
        time.sleep(shutdownTimeout * 60)
        mainStop()
