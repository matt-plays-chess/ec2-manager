This script is used to start/stop EC2 instances. The `start` and `stop` parts are pretty generic, but the `config` step is used to update a configuration file for [SSHEngine](https://gitlab.com/matt-plays-chess/ssh-engine) which is used to connect to a remote chess server.

## Usage

Here are the options for running the script:

```
ec2-manager.py start         starts the instance
ec2-manager.py stop          stops the instance
ec2-manager.py info	         Get details about the instance
ec2-manager.py config        Update the configuration file
ec2-manager.py startconfig   Starts and updates the configuration file
ec2-manager.py fullstart     Starts, configures, and shuts down after a delay
```

## Warning: First Instance Only

I only have 1 EC2 instance so this works for me. But the script is designed to always only return the first instance it finds.

## Credentials

You will need to store your AWS Credentials for the instance outside of this script. See [aws-crendtials.md](aws-credentials.md) for more details.

## Setup

Be sure to change the location of the configuration file in the script before trying to run the `config` step.

## Install dependencies

```
pip install pyyaml
```